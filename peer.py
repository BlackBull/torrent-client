import socket
import bitarray
import binascii
import time
import struct
import math
import hashlib
import logging
import errno

import message
import piece
from customexceptions import PeerException
from customexceptions import TorrentException

logging.basicConfig(level=logging.DEBUG,
					format='[%(levelname)s] (%(threadName)-10s) %(message)s', 
					)

class Peer:

	def __init__(
		 self, ip = None, portnum = None, 
		 handshakemsg = None, torrent = None, 
		 manager = None):
		
		# Message state constants
		self.MSG_TYPE_HANDSHAKE = 1
		self.MSG_TYPE_BITFIELD_OR_HAVE = 2
		self.MSG_TYPE_CHOKE = 3
		self.MSG_TYPE_UNCHOKE = 4
		self.MSG_TYPE_INTERESTED = 5
		self.MSG_TYPE_NINTERESTED = 6
		self.MSG_TYPE_REQUEST = 7
		self.MSG_TYPE_PIECE = 8
		self.MSG_TYPE_KEEP_ALIVE = 9
		self.MSG_TYPE_PIECE_RAW = 80

		# Peer state constants
		self.STATE_PEER_AVILABLE = True
		self.STATE_PEER_CHOKED = True
		self.STATE_PEER_DONE = False
		self.STATE_PEER_PROBLEM = False
		self.STATE_STATE_PEER_PROBLEM_DESC = dict()

		self.ip = ip
		self.portnum = portnum
		self.torrentmeta = torrent
		self.buff = handshakemsg
		self.peer_manager = manager
		self.pieces_count = int(self.torrentmeta.total_file_size / self.torrentmeta.piece_length)
		self.len_of_prev_processed_msg = 0
		self.active_piece_idx = 0
		self.avilable_chunks = bitarray.bitarray()
		self.avilable_chunks.setall(0)
		self.chunks_here = bitarray.bitarray()
		self.chunks_here.setall(0)
		self.expected_message = self.MSG_TYPE_HANDSHAKE
		self.outgoing_message = 0
		self.socket = None
		self.response = None
		self.message_flow = None
		self.transmitting_size = 2**14
		self.pieces_array = None

	def loop(self):
		""" Test commenting """

		while self.expected_message is not self.MSG_TYPE_PIECE:
			self.send_message()
			self.receive_socket_message()
			self.resolve_message_type_handler()
			print(self.response)
		# i = 0
		# while i <= 500:
		# 	if self.expected_message is not self.MSG_TYPE_KEEP_ALIVE:
		# 		self.send_message()
		# 	self.receive_socket_message()
		# 	self.message_flow = self.response
		# 	has_message = True
		# 	msgLen = 68
		# 	# logging.debug(self.message_flow)
		# 	while has_message is True:
		# 		if self.expected_message is not self.MSG_TYPE_HANDSHAKE:
		# 			if len(self.message_flow) > 4:
		# 				msgLen = self.get_current_message_length()
		# 				if self.expected_message is self.MSG_TYPE_PIECE_RAW:
		# 					if self.len_of_prev_processed_msg is not 0:
		# 						self.expected_message = self.MSG_TYPE_PIECE_RAW
		# 				else:
		# 					self.expected_message = self.get_message_type(msgLen, self.message_flow[4])

		# 			else:
		# 				""" Keep-alive """
		# 				self.expected_message = self.get_message_type(msgLen)

		# 		self.resolve_message_type_handler()
		# 		self.process_message_flow(msgLen)


		# 		if len(self.message_flow) < self.get_current_message_length():
		# 			has_message = False
		# 			if self.expected_message is self.MSG_TYPE_BITFIELD_OR_HAVE:
		# 				self.outgoing_message = self.MSG_TYPE_INTERESTED
		# 				# self.create_avilable_blocks_list()
				
		# 		logging.debug(self.avilable_chunks)
		# 		logging.debug(self.chunks_here)

		# 	self.prepare_next_message()
		# 	i+=1

	def expand_pieces_array(self):
		""" Add Piece instances to pieces array """
		ord_num = 0

		for piece_element in self.avilable_chunks:
			piece_val = int(piece_element)
			piece_instance = piece.Piece(piece_val, ord_num, self, 
									self.torrentmeta.piece_length, self.transmitting_size)
			self.pieces_array.append(piece_instance)
			ord_num += 1

			# Expanding array holding all pieces received from client
			self.chunks_here.append(0)

	""" Protocol messages processing """

	def request_bitfield_have(self):
		self.expected_message = self.MSG_TYPE_BITFIELD_OR_HAVE
	
	def handle_handshake(self):
		if self.expected_message is not self.MSG_TYPE_HANDSHAKE:
			raise PeerException(4)

		if not self.validate_handshake():
			raise PeerException(4)

		print("Handshake proof passed.")
		self.prepare_next_message()

		return True

	def validate_handshake(self):
		headerlength = 28

		infohash, raw_infohash = self.torrentmeta.getinfohash()

		received_info_hash = (binascii.hexlify((	
								self.response[headerlength:headerlength+
								len(raw_infohash)])))
		received_info_hash = received_info_hash.decode()
		received_info_hash.strip()
		received_info_hash = received_info_hash.encode("utf-8")
		print(received_info_hash)
		if received_info_hash.decode("utf-8") != infohash.lower():
			return False
		else:
			print("Successfully received handshake from peer.")
			self.request_bitfield_have()
			return True

	def handle_bitfield_and_have(self):
		msgLen = self.get_current_message_length()
		
		# msgLen + 1, because if payload taken from 4th position, 
		# it includes msg ID
		payload = self.message_flow[5:(msgLen)]

		if self.message_flow[4] is 4:
			""" Have """
			hex_payload = binascii.b2a_hex(payload)
			bin_payload = bin(int(hex_payload, 16))[2:]
			self.avilable_chunks[int(bin_payload, 2)] = 1
			if self.pieces_array:
				self.pieces_array[int(bin_payload, 2)].make_avilable()
		elif self.message_flow[4] is 5:
			""" Bitfield """
			hex_payload = binascii.b2a_hex(payload)
			bin_payload = bin(int(hex_payload, 16))[2:]

			self.avilable_chunks.extend(bin_payload)

		if self.pieces_array is None:
			self.pieces_array = []
			self.expand_pieces_array()


	def prepare_next_message(self):
		if self.outgoing_message is self.MSG_TYPE_INTERESTED:
			self.buff = self.generate_interested_message()
		elif self.outgoing_message is self.MSG_TYPE_REQUEST:
			try:
				self.buff = self.generate_request_message()
			except TorrentException as t_except:
				print("TorrentException " + str(t_except))

	def handle_piece_message(self):
		""" Function to handle piece message """
		
		payload = self.message_flow
		msgLen = self.get_current_message_length()
		
		if self.expected_message is self.MSG_TYPE_PIECE_RAW:
			payload = self.process_raw_piece_payload(payload)
		else:
			payload = self.message_flow[5:(msgLen)]
		
		if self.pieces_array[self.active_piece_idx].request_to_add_block(payload):
			if not self.pieces_array[self.active_piece_idx].is_the_piece_avilable():
				self.chunks_here[self.active_piece_idx] = True
		# logging.debug(payload)
		self.len_of_prev_processed_msg = msgLen - len(self.message_flow)

	def process_raw_piece_payload(self, payload):
		""" Payload of prev msg """
		
		if len(payload) - self.len_of_prev_processed_msg == 0:
			""" Requested piece is in current payload """
			return payload
		elif len(payload) - self.len_of_prev_processed_msg > 0:
			""" 
			Current payload contains at least two msgs, 
			including the requested piece from prev msg
			"""
			return payload[:(len(payload) - self.len_of_prev_processed_msg)]
		elif len(payload) - self.len_of_prev_processed_msg < 0:
			""" 
			Current flow contains part of whole message, declared
			in prev message. However, the requested piece is not over
			and is about to be rcvd in future piece msgs
			"""
			return payload

	def get_next_non_downloaded_piece(self):
		for piece_ in self.pieces_array:
			if self.peer_manager.request_piece_for_processing(piece_.get_piece_idx()):
				if piece_.get_piece_ct() is 1:
					if piece_.is_the_piece_avilable():
						if not self.chunks_here[piece_.get_piece_idx()]:
							self.active_piece_idx = piece_.get_piece_idx()
							return piece_.get_piece_idx()
		return None

	def generate_request_message(self):		
		if self.STATE_PEER_CHOKED is True:
			raise PeerException(55)

		next_piece_idx = self.get_next_non_downloaded_piece()
		
		# logging.debug("EXPECTING PIECE")
		# logging.debug(self.active_piece_idx)

		if next_piece_idx is None:
			print("The whole torrent was downloaded")
			self.peer_manager.write_content_from_peer_to_file(self.pieces_array)
			raise TorrentException(1)
		
		next_block_idx = self.pieces_array[next_piece_idx].get_next_avilable_block()
		requested_message_size = self.pieces_array[next_piece_idx].get_size_of_downloading_block()
		requested_message_size = int(requested_message_size)


		block_size = self.torrentmeta.piece_length / self.pieces_array[next_block_idx].count_of_blocks
		block_size = int(block_size)

		# Request
		metalen = struct.pack(">L", 13)
		msgid = binascii.a2b_qp(chr(6))
		current_index = struct.pack(">L", next_piece_idx)
		begin = struct.pack(">L", next_block_idx * block_size)
		length = struct.pack(">L", int(requested_message_size))
		request_message = metalen + msgid + current_index + begin + length
		self.expected_message = self.MSG_TYPE_PIECE
		return(request_message)
		
	def generate_interested_message(self):
		interested_message = chr(0)*3 + chr(1)*1 + chr(2)*1
		interested_message = binascii.a2b_qp(interested_message)
		return(interested_message)
		
	def handle_keepalive(self):
		""" Keep-alive """
		print("Keep alive")

	def handle_choke(self):
		""" Choke """
		self.STATE_PEER_CHOKED = True
		self.outgoing_message = self.MSG_TYPE_INTERESTED

	def handle_unchoke(self):
		""" Unchoke """
		self.STATE_PEER_CHOKED = False
		self.outgoing_message = self.MSG_TYPE_REQUEST


	""" Message pack processing """

	def get_current_message_length(self):
		if self.expected_message is not self.MSG_TYPE_PIECE_RAW:
			msgLen = int.from_bytes(self.message_flow[:4], byteorder = "big")
			return msgLen + 4
		else:
			msgLen = self.len_of_prev_processed_msg - len(self.message_flow)
			if msgLen < 0:
				return len(self.message_flow)
			return msgLen

	def process_message_flow(self, past_message_length):
		self.message_flow = self.message_flow[past_message_length:]

	def convert_dec_to_bin(self, dec):
		pieces = bin(dec)[2:]
		if len(pieces) is not 8:
			complement_bits = (8 - len(pieces)) * '0'
			pieces = complement_bits + pieces
		return pieces

	def get_message_type(self, msgLen, msgType = -1):
		if msgLen is 4:
			return self.MSG_TYPE_KEEP_ALIVE

		if msgType is 0:
			return self.MSG_TYPE_CHOKE
		elif msgType is 1:
			return self.MSG_TYPE_UNCHOKE
		elif msgType is 2:
			return self.MSG_TYPE_INTERESTED
		elif msgType is 3:
			return self.MSG_TYPE_NINTERESTED
		elif msgType is 4:
			return self.MSG_TYPE_BITFIELD_OR_HAVE
		elif msgType is 5:
			return self.MSG_TYPE_BITFIELD_OR_HAVE
		elif msgType is 6:
			return self.MSG_TYPE_REQUEST
		elif msgType is 7:
			return self.MSG_TYPE_PIECE
		else:
			try:
				if self.len_of_prev_processed_msg is not 0:
					return self.MSG_TYPE_PIECE_RAW
				raise PeerException(3)
			except PeerException as exc:
				pass

	def resolve_message_type_handler(self):
		if self.expected_message is self.MSG_TYPE_HANDSHAKE:
			self.handle_handshake()
		elif self.expected_message is self.MSG_TYPE_BITFIELD_OR_HAVE:
			self.handle_bitfield_and_have()
		elif self.expected_message is self.MSG_TYPE_CHOKE:
			self.handle_choke()
		elif self.expected_message is self.MSG_TYPE_UNCHOKE:
			self.handle_unchoke()
		elif self.expected_message is self.MSG_TYPE_KEEP_ALIVE:
			self.handle_keepalive()
		elif self.expected_message is self.MSG_TYPE_PIECE:
			self.handle_piece_message()
		elif self.expected_message is self.MSG_TYPE_PIECE_RAW:
			self.handle_piece_message()
		else:
			self.STATE_PEER_DONE = True

	""" Connection-related operations """

	def connect(self):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self.socket.connect((self.ip, int(self.portnum)))
		except OSError as err:
			self.peer_manager.thread_problem = 1
			raise PeerException(111)
		else:
			print("Successful connection to peer.")
			self.loop()

	def send_message(self):
		try:
			self.socket.send(self.buff)
		except OSError as err:
			raise PeerException(32)
		else:
			print("Message was successfully sent.")
	
	def receive_socket_message(self):
		try:
			self.response = self.socket.recv(self.transmitting_size)
		except OSError as err:
			raise PeerException(32)
		else:
			print("Successfully received response from peer")