import bitarray
import logging
import threading

import peer
import customexceptions

logging.basicConfig(level=logging.DEBUG,
					format='[%(levelname)s] (%(threadName)-10s) %(message)s', 
					)

class PeerManager:

	def __init__(self, torrent, files):
		self.torrent = torrent 
		self.peers = []
		self.filesToWrite = files
		self.handshakemsg = None
		self.total_pieces = int(self.torrent.total_file_size / self.torrent.piece_length) * 2
		self.total_pieces_ct = bitarray.bitarray(self.total_pieces)
		self.processing_pieces = bitarray.bitarray(self.total_pieces)
		self.processing_pieces.setall(0)
		self.piece_to_process_lock = threading.Lock()
		self.peer_threads = []
		self.thread_problem = 0

		self.createpeers()
		self.connect()

	def request_piece_for_processing(self, piece_idx):
		print("Processed piece")
		print(piece_idx)
		if self.processing_pieces[piece_idx] is 1:
			""" 
			Piece is being processed by 
			another peer or is already downloaded 
			"""
			return False
		else:
			""" 
			arr[idx] is 0, means it's not downloaded or
			being processed
			"""
			with self.piece_to_process_lock:
				self.processing_pieces[piece_idx] = 1
			return True

	def write_content_from_peer_to_file(self, pieces_from_peer):
		buff = ""
		for piece in pieces_from_peer:
			for block in piece.content:
				if block is None:
					continue
				buff += block.decode()

		self.filesToWrite[0].add_content(buff.encode())
		self.filesToWrite[0].write_in_file()
		

	def write_chunk_to_file(self, chunk):
		# TODO: Check which file is to be opened and edited
		self.filesToWrite[0].add_content(chunk[11:])
		self.filesToWrite[0].write_in_file()

	def connect(self):
		for peerObj in self.peers:
			peer_thread = threading.Thread(target = peerObj.connect)
			self.peer_threads.append(peer_thread)
			
		for peer_thread in self.peer_threads:
			self.thread_problem = 0
			peer_thread.start()
			if self.thread_problem is 1:
				self.peers.remove(peerObj)

	def createpeers(self):
		if self.handshakemsg is None:
			self.handshakemsg = self.torrent.generatehandshakemsg()
		for rcvdpeer in self.torrent.getpeers():
			peer_ip, peer_port = self.parsepeerinfo(rcvdpeer)
			newpeer = peer.Peer(peer_ip, peer_port, self.handshakemsg, self.torrent, self)
			self.peers.append(newpeer)

	def parsepeerinfo(self, peer):
		peer_tmp = peer.split(":")
		peer_info = []
		peer = peer.replace(peer_tmp[-1], "")
		peer_info.append(peer)
		peer_info[0] = peer_info[0][:-1]
		peer_info.append(peer_tmp[-1])
		return peer_info[0], peer_info[1]

	def manage_exception(self, err_no):
		print("Will manage exception")