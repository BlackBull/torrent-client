import asyncio
import datetime

@asyncio.coroutine
def learn_time(loop):
	reader, writer = yield from asyncio.open_connection("127.0.0.1", 52132, 
														loop = loop)
	received_data = yield from reader.read(-1)
	print("It's %r" % received_data.decode())

	writer.close()

loop = asyncio.get_event_loop()
loop.run_until_complete(learn_time(loop))
loop.close()