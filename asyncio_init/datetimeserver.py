import asyncio
import datetime

@asyncio.coroutine
def tell_current_date(reader, writer):
	current_date = datetime.date.today()

	writer.write(current_date.encode())
	# yield from writer.drain()

	writer.close()

loop = asyncio.get_event_loop()
opSt = asyncio.start_server(tell_current_date, "127.0.0.1", 52132, loop = loop)
server = loop.run_until_complete(opSt)

print("Serving on {}".format(server.sockets[0].getsockname()))
try:
	loop.run_forever()
except KeyboardInterrupt:
	pass

server.close()
loop.run_until_complete(server.wait_closed())
loop.close()