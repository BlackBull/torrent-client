import socket

class LocalhostClient:
	
	def __init__(self, sock = None):
		self.hostname = None
		self.portnum = None
		
		if sock is None:
			self.sock = socket.socket(socket.AF_INET,
									  socket.SOCK_STREAM)
		else:
			self.sock = sock

	def connect(self, Client, port):
		self.sock.connect((Client, port))
		self.hostname = Client
		self.portnum = port

	def sendrequest(self, content = None):
		if content is None:
			raise NameError("Content cannot be empty")
		else:
			updatedcontent = self.hostname + ":" + repr(self.portnum)
			self.sock.sendall(b"UPDATE index.html HTTP/1.1/\r\nHost: localhost:8080/\r\nContent-Type: text/html/\r\n<h1>This is a testing content for the text file foo.txt</h1>")

localhostClient = LocalhostClient()
localhostClient.connect("localhost", 8080)
localhostClient.sendrequest("Bla")