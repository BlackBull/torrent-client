import asyncio

@asyncio.coroutine
def handle_echo(loop, message):
	reader, writer = yield from asyncio.open_connection('127.0.0.1', 42325, 
											loop = loop)

	print("Send %r" % message)
	writer.write(message.encode())

	received_data = yield from reader.read(-1)
	print("Received: %r" % received_data.decode())

	writer.close()



message = "Hello"
loop = asyncio.get_event_loop()
loop.run_until_complete(handle_echo(loop, message))
loop.close()