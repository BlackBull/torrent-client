import binascii
import random
import bencodepy
import hashlib
import collections
import urllib.request
import urllib
import socket

import filemanager
import peermanager
from customexceptions import TorrentException

class Torrent:

	def __init__(self, 
			torrent_location = None, target_folder = None):
		self.torrent_location = torrent_location
		self.target_folder = target_folder
		self.items = list()
		self.fileInstances = list()
		self.piece_length = 2**15
		self.total_file_size = 132447		# Bytes
		self.bdecoded_metafile = collections.OrderedDict()
		self.peer_id = None

		self._parse_torrent()
		self.create_file_instances()
 
		self.peerManager = peermanager.PeerManager(self, self.fileInstances)

	def create_file_instances(self):
		for fileName in self.get_file_names():
			fileInstance = filemanager.File(fileName, self.target_folder)
			self.fileInstances.append(fileInstance)
	
	def generatehandshakemsg(self):
		handshakemsg = chr(19) + "BitTorrent protocol" + 8*chr(0)
		handshakemsg = binascii.a2b_qp(handshakemsg.encode("utf-8"))
		if self.peer_id is None:
			self.generateclientid()
		client_id = self.peer_id
		peer_id = binascii.a2b_qp(client_id.encode("utf-8"))
		infohash, rawinfohash = self.getinfohash()
		return(handshakemsg + rawinfohash + peer_id)

	def generateclientid(self):
		CLIENT_NAME = "4B" 
		CLIENT_VERSION = "01A"
		clientID = [random.randint(0,9) for p in range(0,12)]
		clientID = ''.join(map(str, clientID))
		self.peer_id = "-" + CLIENT_NAME + "-" + CLIENT_VERSION + "-" + clientID
	
	def get_files(self):
		return self.fileInstances

	def getpeers(self):
		""" 
		TODO: Process situation of announce_url = None
		"""
		announce_url = self.get_announce_url()
		info_hash, raw_info_hash = self.getinfohash()
		if self.peer_id is None:
			self.generateclientid()
		port = 6888
		uploaded = "0"
		downloaded = "0"
		left = 706700230 + 5465566 + 1646

		params = {
			"info_hash": raw_info_hash,
			"peer_id": self.peer_id,
			"port": port,
			"uploaded": uploaded,
			"downloaded": downloaded,
			"left": left,
			"compact": 1
		}

		return [
			"77.70.100.163:20222"
		]

	def get_announce_url(self):
		announce_url = self._return_idx_of_bencoded_property("announce")
		if announce_url is None:
			""" No tracker presented """
			try:
				raise TorrentException(71)
			except TorrentException as te:
				pass
			
			return None
		announce_url = self.items[announce_url][1].decode()
		return(announce_url)

	def getinfohash(self):
		info_idx = self._return_idx_of_bencoded_property("info")
		info_decoded = self.items[info_idx][1]
		bencoded_info = bencodepy.encode(info_decoded)
		info_hash = hashlib.sha1(bencoded_info).digest()
		info_hash = binascii.hexlify(info_hash).decode()
		rawinfohash = bytes.fromhex(info_hash)

		return info_hash, rawinfohash
	
	def get_file_names(self):
		file_names = []
		info_idx = self._return_idx_of_bencoded_property("info")
		
		info_idx = self._return_idx_of_bencoded_property("info")
		name_idx = self._return_idx_of_bencoded_property("name", self.items[info_idx][1])
		target_folder = self.items[info_idx][1]["name".encode()].decode()
		files_idx = self._return_idx_of_bencoded_property("files", self.items[info_idx])
		
		if files_idx is None:
			""" Only one file is being shared """
			file_names.append(target_folder)
			target_folder = "."
		else:
			info_arr = self.items[info_idx][files_idx]
			files_arr = info_arr["files".encode()]
			
			for it in range(0, len(files_arr)):
				file_names.append(files_arr[it]["path".encode()])

		return file_names
		
	def _return_idx_of_bencoded_property(self, searched_item, 
					opt_array = None):
		""" 
		Returns index of a selected element within the whole
		bencoded property 
		"""
		if opt_array is None:
			elements_array = self.items
		else:
			elements_array = opt_array
		
		i = 0
		
		for element in elements_array:
			if searched_item.encode() in element:
				return i
			else:
				i+=1
		return None

	def _parse_torrent(self):
		self.bdecoded_metafile = bencodepy.decode_from_file(self.torrent_location)
		self.items = list(self.bdecoded_metafile.items())