class TorrentException(BaseException):

	def __init__(self, err):
		self.exceptions = {
			1: self.torrent_finished,
			71: self.missing_announce_uri
		}
		self.handle_exception(err)

	def torrent_finished(self):
		""" Nothing more to be downloaded """
		print("Torrent has been downloaded.")

	def missing_announce_uri(self):
		""" Process missing announce URI """
		print("Missinng announce URL")

	def handle_exception(self, err_no):
		self.exceptions[err_no]()


class BencodingException(BaseException):

	def __init__(self, err):
		exceptions = {
			5: self.structure_not_dictionary
		}
		self.handle_exception(err)

	def structure_not_dictionary(self):
		""" 
		Element was requested to be added to
		dictionary but structure not dictionary 
		"""
		print("Element was requested to be added to " + 
			"dictionary but structure not dictionary")

	def handle_exception(err_no):
		exceptions[err_no]()


class FileException(BaseException):

	def __init__(self, err):
		self.exceptions = {
			23: self.declined_dependencies()
		}
		self.handle_exception(err)
	
	def declined_dependencies(self):
		""" User declined installing dependencies """
		print("Declined installing dependencies, can't proceed")

	def handle_exeption(self, err_no):
		self.exceptions[err_no]()


class PeerException(BaseException):

	def __init__(self, err):
		self.exceptions = {
			1: self.peer_done,
			2: self.received_fake_block,
			3: self.received_unknown_msg,
			4: self.received_fake_handshake_msg,
			32: self.brokenpipe,
			55: self.sending_while_choked,
			111: self.connectionrefused
		}
		self.handle_exception(err)

	def peer_done(self):
		""" Peer is done """
		return None

	def received_fake_block(self):
		""" Remove bad peer since sent fake block """
		return None

	def received_unknown_msg(self):
		""" Received unknown message type """
		print("Received unknown message type")
		return None

	def received_fake_handshake_msg(self):
		""" Received wrong handshake """
		print("Received wrong handshake message")
		return None

	def brokenpipe(self):
		print("Client not avilable. Broken Pipe [Errno32]. Removing bad peer...")
		return None
	
	def sending_while_choked(self):
		""" Send interested message """ 
		print("Sending while choked")
	
	def connectionrefused(self):
		print("Connection refused on selected port. Connection Refused [Errno111]. Removing bad peer...")
		return None

	def handle_exception(self, err_no):
		self.exceptions[err_no]()