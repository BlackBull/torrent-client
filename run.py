import sys
import argparse

import peermanager
import peer
import torrentmanager
import filemanager

arguments = sys.argv
parser = argparse.ArgumentParser()
parser.add_argument("<torrent_file>", help="Location of the .torrent file")
parser.add_argument("<target_folder>", help="Location where the file would be downloaded")
parser.parse_args()

torrent_file = arguments[1]
target_folder = arguments[2]
torrentManager = torrentmanager.Torrent(torrent_file, target_folder)