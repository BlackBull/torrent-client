import logging
import threading
import bitarray
import time

logging.basicConfig(level=logging.DEBUG,
					format='[%(levelname)s] (%(threadName)-10s) %(message)s',
					)

idx = 0
arr = bitarray.bitarray(5)
arr.setall(0)

def fill_array():
	global arr
	global idx

	logging.debug("Starting")

	for idx, el in enumerate(arr):
		if el is False:
			arr[idx] = True
		
		logging.debug(arr)

t = threading.Thread(target = fill_array)
t2 = threading.Thread(target = fill_array)

t.start()
t2.start()