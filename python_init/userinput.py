x = int(input("Please, enter an integer: "))
if x < 0:
	x = 0
	print("Nothing interesting, x changed to zero")
elif x == 0:
	x = 1
	print("X was zero, changed to one")
elif x >= 0:
	x = 0
	print("x = 0")
else:
	x = 2
	print("x = 2")