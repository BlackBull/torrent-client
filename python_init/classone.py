class ClassOne:

	def __init__(self):
		self.samplevar = None
		self.anothersamplevar = None

	def setsamplevar(self, value):
		self.samplevar = value

	def setanothersamplevar(self, value):
		self.anothersamplevar = value

	def getsamplevar(self):
		return self.samplevar

	def getanothersamplevar(self):
		return self.anothersamplevar