def cheeseshop(kind, *arguments, **keywords):
	print("- Excuse me sir, do you have any ", kind, "?")
	print("- No, sadly we've run out of", kind, ".")
	for arg in arguments:
		print("-", arg)
	print("-" * 40)
	keys = sorted(keywords.keys())
	for kw in keys:
		print(kw, ":", keywords[kw])

cheeseshop("Cheeseburger", "It's very funny, sir.",
			"It's really very, VERY funny.", 
			shopkeeper = "Michael Palin", 
			client = "John Cheese", 
			shop = "Valins' Grocery Store")