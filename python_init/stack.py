class ExampleStackClass():
	"""docstring for stack"""

	__container = []

	def push(self, digit):
		self.__container.append(digit)

	def pop(self):
		del self.__container[-1]

	def printct(self):
		print(self.__container)

	def __init__(self):
		super(ExampleStackClass, self).__init__()

stackobject = ExampleStackClass()
stackobject.push(3)
stackobject.push(6)
stackobject.push(9)
stackobject.push(13)
stackobject.printct()
stackobject.pop()
stackobject.printct()