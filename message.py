import binascii
import struct

# import getpeers

def process(peer, self_peer_id, torrentmeta, sent_message):
	""" Message determination function """
	

	if peer.handshake is True:
		""" Checking received handshake whether valid """
		
		if not checkinfohash(peer, torrentmeta):
			return False

		if (len(sent_message) < len(peer.buff)):
			""" Receiving information about the pieces a peer has """
			geninfo = peer.buff[len(sent_message):]
			print(geninfo)
			determine_message(peer, geninfo)
			print(peer.bitfield)

	return True

def bitfield(peer, message):
	msgsize = message[:3]
	bitfield = message[6:]
	print(bitfield[0])
	counter = 0
	for piece in bitfield:
		processbyte(peer, piece)
	# print(counter)


def processbyte(peer, piece):
	""" Append bit to the bitfield structure """

	pieces = bin(piece)[2:]
	if len(pieces) is not 8:
		complement_bits = (8 - len(pieces)) * '0'
		pieces = complement_bits + pieces
	for bit in pieces:
		peer.bitfield.append(int(bit))
	
	
def determine_message(peer, message):
	return {
		5: bitfield(peer, message)
	}[message[4]]


def checkinfohash(peer, torrentmeta):
	""" 
	Checks whether the received handshake's
	info hash matches the torrent's one
	"""

	headerlength = 28

	received_info_hash = (binascii.hexlify((	
							peer.buff[headerlength:headerlength+
							len(torrentmeta.getinfohashraw())])))
	received_info_hash = received_info_hash.decode()
	received_info_hash.strip()
	received_info_hash = received_info_hash.encode("utf-8")
	if received_info_hash != (torrentmeta.getinfohash().encode("utf-8")):
		return False
	else:
		print("Successfully received handshake from peer.")
		peer.handshake = False
		return True