"Bencoding class"
"d3:cow3:moo4:spam4:eggse # => { \"cow\" => \"moo\", \"spam\" => \"eggs\" }"
"d4:spaml1:a1:bee # => { \"spam\" => [ \"a\", \"b\" ] } "
import binascii

import customexceptions

class Bencode:

	def __init__(self, torrent_file):
		self.torrent_file = torrent_file
		self.curr_idx = 0
		self.file_stream = None
		self.structure = None
		self.current_structure = 0
		self.current_global_str_position = 0
		self.current_local_str_position = 0
		self.tmp_structures_list = []
		self.tmp_dictionary_ct = []
		self.dict_el = 0

	def add_information_to_dictionary_tmp_structure(self, data):

		if self.dict_el is 0:
			self.tmp_dictionary_ct = []

		self.dict_el += 1
		self.tmp_dictionary_ct.append(data)

		if self.dict_el > 1:
			self.tmp_structures_list[self.current_local_str_position][self.tmp_dictionary_ct[0]] = self.tmp_dictionary_ct[1]
			self.dict_el = 0


	def create_list(self):
		self.current_structure = 2
		self.tmp_structures_list.append([])
		print("List")

	def create_dictionary(self):
		# if self.current_global_str_position is 0:
		# 	self.structure = {}
		# 	self.current_global_str_position += 1
		# 	self.tmp_structures_list.append({})
		# else:
		self.current_structure = 1
		self.dict_el = 0
		self.tmp_structures_list.append({})
		self.current_local_str_position = 0

	def create_integer(self):
		self.current_structure = 3

	def close_current_structure(self):
		self.current_structure = 0
		
	def get_data_contents_for_structure(self, 
										data_beginning_idx, 
										data_len):

		data_beginning_idx += 1
		data_len += data_beginning_idx
		data = self.file_stream[data_beginning_idx:data_len].decode()
		if self.current_structure is 1:
			self.add_information_to_dictionary_tmp_structure(data)
		elif self.current_structure is 2:
			self.tmp_structures_list[self.current_local_str_position].append(data)
		elif self.current_structure is 3:
			integer_slice = self.file_stream[data_beginning_idx:]
			integer_ending = integer_slice.index("e".encode())
			integer_slice = integer_slice[:integer_ending]
			self.add_information_to_dictionary_tmp_structure(integer_slice.decode())
			# self.tmp_structures_list[self.current_local_str_position][self.tmp_dictionary_ct[0]] = integer_slice.decode()

	def create_according_data_structure(self, element):
		options = {
			"l": self.create_list,
			"d": self.create_dictionary,
			"i": self.create_integer,
			"e": self.close_current_structure
		}
		if element in options:
			options[element]()

	def analyze_meta_data(self):
		if self.file_stream is None:
			raise customexceptions.BencodingException(1)

		while self.curr_idx <= len(self.file_stream):
			sliced_stream = self.file_stream.decode()
			sliced_stream = sliced_stream[self.curr_idx:]
			local_index = 0
			if sliced_stream[local_index] is not "i":
				data_meta = sliced_stream[:len(self.file_stream)]
				data_len = int(data_meta[self.curr_idx+1:])
				data_beginning_idx = 0
			else:
				try:
					data_beginning_idx = sliced_stream.index(":")
				except ValueError as err:
					data_beginning_idx = 0
					data_meta = sliced_stream[:len(self.file_stream)]
					data_len = 0
				else:
					data_meta = sliced_stream[:data_beginning_idx]
					if sliced_stream[local_index].isalpha():
						data_len = 0
					else:
						data_len = int(data_meta)

			self.create_according_data_structure(data_meta[local_index])
			self.get_data_contents_for_structure(data_beginning_idx + self.curr_idx, data_len)
			
			self.curr_idx += len(data_meta) + 1 + data_len


	def process_torrent_file(self):
		self.analyze_meta_data()

	def open_torrent_file(self):
		self.file_stream = open(self.torrent_file, "rb")
		self.file_stream = self.file_stream.read()


b = Bencode("./tmp/chill_torrent.torrent")
b.open_torrent_file()
b.process_torrent_file()
print(b.tmp_structures_list)