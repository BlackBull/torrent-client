import binascii

import peer
import customexceptions

class Piece:
	""" 
	Class storing information 
	and methods for each piece
	instead of array for all the pieces
	and operations in Peer class
	"""

	def __init__(self, content, piece_idx, peer, 
					piece_length, transmitting_size):
		
		# Piece avilable is true if piece is avilable in peer 
		self.piece_avilable = content
		
		self.piece_idx = piece_idx
		self.piece_length = piece_length
		self.transmitting_size = transmitting_size
		self.count_of_blocks = int(self.piece_length / self.transmitting_size)
		self.peer_info = peer
		self.content = [None] * self.count_of_blocks	# int
		self.currently_transmitted_block = 0
		
		# Block is false if piece is avilable within peer but is not downloded
		self.blocks = []	# int
		self.blocks_so_far = 0

		self.create_avilable_blocks_list()

		if self.count_of_blocks == 0:
			""" Prevents from case of nothing in the piece """
			self.count_of_blocks = 1


	def request_to_add_block(self, content):
		""" Request block to be added to piece """

		if not self._check_if_block_valid():
			raise customexceptions.PeerException(2)

		self.content[self.currently_transmitted_block] = content
		self.blocks_so_far += 1
		self.blocks[self.currently_transmitted_block] = 1

		return True
		
		
	def _check_if_block_valid(self):
		""" """
		# TODO Checks if received chunk matches
		return True

	def is_the_piece_avilable(self):
		""" Check if piece has to be downloded """
		if self.piece_avilable:
			try:
				if self.blocks.index(0) is not None:
					return True
			except ValueError as err:
				pass

		return False

	def get_piece_idx(self):
		return self.piece_idx

	def get_piece_ct(self):
		return self.piece_avilable

	def get_next_avilable_block(self):
		""" Get next block within the piece """

		for block_idx in range(0, self.count_of_blocks):
			if self.blocks[block_idx] is 0:
				self.currently_transmitted_block = block_idx
				return block_idx
		return None

	def get_size_of_downloading_block(self):
		block_size = self.piece_length / self.count_of_blocks
		downloaded_blocks_size = self.blocks_so_far * block_size
		if downloaded_blocks_size + block_size <= self.piece_length:
			return block_size
		else:	
			""" If block size == 0 => piece was downloaded """
			return 0

	def create_avilable_blocks_list(self):
		""" Number of blocks per piece """

		if self.piece_avilable is 1:
			for piece in range(0, self.count_of_blocks):
				self.blocks.append(0)

	def make_avilable(self):
		self.piece_avilable = 1
		if not self.blocks:
			self.create_avilable_blocks_list()