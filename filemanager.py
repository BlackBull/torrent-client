import binascii
import bitarray

import customexceptions

class File:

	def __init__(self, name = None, target_folder = None):
		""" Constructor """
		
		self.STATE_FILE_OPENED = False

		self.name = name
		self.target_folder = target_folder
		self.buff = bytearray()
		self.fileToWrite = None
		self.avilablePieces = bitarray.bitarray()

	def add_content(self, content):
		self.buff = content

	def write_in_file(self):
		if self.STATE_FILE_OPENED is True:
			raise FileException(1)
		self.open_file()
		self.fileToWrite.write(self.buff)
		self.close_file()
		print("Successfully written to file")

	def open_file(self):
		self.fileToWrite = open("./" + str(self.target_folder) + 
									"/" + str(self.name), "wb")
		self.STATE_FILE_OPENED = True

	def close_file(self):
		self.fileToWrite.close()
		self.STATE_FILE_OPENED = False

	def get_name(self):
		return(self.name)

	def get_state_of_file(self):
		return self.STATE_FILE_OPENED